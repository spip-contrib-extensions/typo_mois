<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'typo_dates_description' => 'Ce plugin propose un filtre qui reconnait les dates au format "10 Mars" et remplace l\'espace entre le jour et le mois par un espace ins&#233;cable.
Par exemple, <code>"1er mars"</code> devient <code>"1er&nbsp;mars"</code>. Il gère aussi l\'affichage des siècles <code>"XXe siècle"</code> devient <code><span class="siecle">XX<sup>e</sup></span>&nbsp;siècle</code>
',
	'typo_dates_nom' => 'Typo DATES',
	'indexer_slogan' => 'Afficher les dates en français',
);

?>