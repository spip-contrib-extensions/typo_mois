<?php

function typo_dates_pre_typo($texte){
	// gérer les mois
	static $typo;
	if(!$typo) {
		$typo = array();
		for ($m=1; $m<=12; $m++)
			$typo[] = _T('date_mois_'.$m);
		$pre1 = _T('date_jnum1');
		$pre2 = _T('date_jnum2');
		$pre3 = _T('date_jnum3');
		$typo = ",([1-3]?[0-9]|$pre1|$pre2|$pre3) (".join('|', $typo).')\b,UimsS';
		include_spip('inc/charsets');
		$typo = unicode2charset(html2unicode($typo));
	}

	$texte = preg_replace($typo, '\1&nbsp;\2', $texte) ;

	// gérer les siècles
	include_spip('inc/charsets');
	if (!defined('_TYPO_class')) {
		define('_TYPO_class', '<sup class="typo_exposants">');
	}
	
	return preg_replace_callback(
		'`\s([IVX]+)(?:<sup>)?(e)(?:</sup>)?(?:\p{Zs}|~)(siècle)`u',
		function ($matches) {
			return ' <span class="siecle">' . $matches[1] . _TYPO_class . 'e</sup></span>&nbsp;siècle';
		},
		$texte
	);
}

function typo_dates_insert_head_css($flux) {
	// Ajouter des styles CSS directement
	$flux .= '<style>
		.siecle {
			font-variant: small-caps;
			text-transform: lowercase;
		}
		.siecle sup {
			font-variant: normal;
		}
	</style>';
	return $flux;
}
